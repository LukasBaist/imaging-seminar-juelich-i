# ---------------------------------
# Imports
# ---------------------------------
import cv2
import numpy as np
import os
import math


# ---------------------------------
# Functions
# ---------------------------------

# Routine that shifts the images to align them, using a dark spot that is visible in each
# image as a fix-point
def fix_image(image_name):
    image = cv2.imread(image_name)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Zooming the image to get the coordinates of a dark spot for orientiation
    dark_spot = np.zeros((150 , 150))
    for j in range(150):
        for k in range(150):
            dark_spot[j][k] = gray[j+1630][k+2590]

    max_valuesX = []
    max_valuesY = []

    for i in range(20):
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(dark_spot)
        dark_spot[min_loc[1]][min_loc[0]] = 255
        max_valuesX.append(min_loc[0])
        max_valuesY.append(min_loc[1])

    x = math.floor(np.mean(max_valuesX))
    y = math.floor(np.mean(max_valuesY))

    # Going from the dark spot do each direction with fixed steps
    fixed_image = np.zeros((2380, 3122, 3), dtype="int32")
    if (fix_image.previous != (-1,-1)) and (abs(fix_image.previous[0] - x) >= 2 \
            or abs(fix_image.previous[1] - y) >= 2):
        fixed_image[0:2380, 0:3122] = image[fix_image.previous[1]:fix_image.previous[1] + 2380, fix_image.previous[0]:fix_image.previous[0] + 3122]
    else:
        fixed_image[0:2380, 0:3122] = image[y:y + 2380, x:x + 3122]
        fix_image.previous = (x, y)

    cv2.imwrite("Lab_shifted/" + image_name, fixed_image)

# Routine that tries to create a directory used to store the images
def createDir(path):
    try:
        os.mkdir(path)
    except:
        i = 1

		
# ---------------------------------
# Main program
# ---------------------------------	

# Create directories
createDir("Lab_shifted")
createDir("Lab_shifted/20190724")
createDir("Lab_shifted/20190725")
createDir("Lab_shifted/20190724/exercise_1_with_sample")
createDir("Lab_shifted/20190724/exercise_2_with_sample")
createDir("Lab_shifted/20190724/exercise_3_with_sample_with_wp")
createDir("Lab_shifted/20190724/exercise_3_with_sample_without_wp")
createDir("Lab_shifted/20190725/exercise_4_with_sample_with_L4")
createDir("Lab_shifted/20190725/exercise_4_with_sample_with_WP_ontop_L4")

# Apply the shift-correction to each measurement

fix_image.previous = (-1,-1)

for i in range(18):
    try:
        image_name = "20190724/exercise_1_with_sample/img_" + str(i * 10) + ".tiff"
        fix_image(image_name)
        print(image_name + ": Done!")
    except:
        print(image_name + ": Not found!")

fix_image.previous = (-1,-1)

for i in range(1,18,2):
    for j in range(18):
        try:
            image_name = "20190724/exercise_2_with_sample/img_lp_" + str(i * 10) + "_wp_" + str(j*10) + ".tiff"
            fix_image(image_name)
            print(image_name + ": Done!")
        except:
            print(image_name + ": Not found!")

fix_image.previous = (-1,-1)

for i in range(1,18,2):
    for j in range(18):
        try:
            image_name = "20190724/exercise_3_with_sample_with_wp/img_lp_" + str(i * 10) + "_wp_" + str(j*10) + ".tiff"
            fix_image(image_name)
            print(image_name + ": Done!")
        except:
            print(image_name + ": Not found!")

fix_image.previous = (-1,-1)

for i in range(18):
    image_name = "20190724/exercise_3_with_sample_without_wp/img_" + str(i * 10) + ".tiff"
    fix_image(image_name)
    print(image_name + ": Done!")


fix_image.previous = (-1,-1)

for i in range(1,18,2):
    for j in [0, 45, 90, 135]:
        try:
            image_name = "20190725/exercise_4_with_sample_with_L4/img_lp_" + str(i * 10) + "_L4_" + str(j) + ".tiff"
            fix_image(image_name)
            print(image_name + ": Done!")
        except:
            print(image_name + ": Not found!")

fix_image.previous = (-1,-1)

for i in [0, 90]:
    for j in [0, 45, 90]:
        for k in [0, 40, 90, 130]:
            try:
                image_name = "20190725/exercise_4_with_sample_with_WP_ontop_L4/img_lp_" + str(i) + "_L4_" + str(j) + "_wp_" + str(k) + ".tiff"
                fix_image(image_name)
                print(image_name + ": Done!")
            except:
                print(image_name + ": Not found!")


