# ---------------------------------------- #
# Imports
# ---------------------------------------- #

from PIL import Image

import numpy as np
import lmfit
import matplotlib.pyplot as plt


# ---------------------------------------- #
# Setup
# ---------------------------------------- #

directory = "Arrays/Shifted/Background/"
directory_S = "Arrays/Shifted/Sample/"
directory_D = "Arrays/Shifted/Difference/"
appendix = ".npy"

figsize_x = 18
figsize_y = 12

yerr = 2
xerr = 1

pixels = []
pixels.append([1 ,  324,  972])
pixels.append([2 , 1215,   63])
pixels.append([3 , 1283,  295])
pixels.append([4 , 2889, 1090])
pixels.append([5 , 3027, 2302])
pixels.append([6 , 2973, 2379])
pixels.append([7 ,  693, 1242])
pixels.append([8 , 2133, 1325])
pixels.append([9 , 1675,  691])
pixels.append([10, 1477, 1420])

steps_20 = range(10,180,20)
steps_45 = range(0,180,45)
steps_45_2 = range(0,135,45)
steps_90 = range(0,180,90)

channels = [0,1,2]

get_ipython().magic('matplotlib inline')

mask_background_blue = np.arange(0,180,10) < 140
mask_background_blue_inv = np.arange(0,180,10) >= 140

props = dict(boxstyle='round', facecolor='white', alpha=0.5)


# ---------------------------------------- #
# Functions
# ---------------------------------------- #

# The fitting function (sinusoidal)
def fitfun(x, A, B, C, phi):
    return A*np.sin(B*x + phi) + C

# Method that fits the data stored at the given path and saves the fit-plot as well as the parameter-dictionary
def fitData(path, xlabel, title, savepath_1, savepath_2, savepath_dict):
    
    arr = np.load(path)
    
    l = []
    
    # Loop over all pixels
    for i in range(0,10):
    
        temp = []
    
        # Read in the data
        x = arr[i,:,0]
        y_red = arr[i,:,1]
        y_green = arr[i,:,2]
        y_blue = arr[i,:,3]
        
        # Fit the data individually for each channel, using the fit-function
        # Provide first-guesses and constraints to the fit-method (lmfits least-square)
        model_red = lmfit.model.Model(fitfun)
        pars_red = model_red.make_params()
        pars_red["A"].set(30)
        pars_red["B"].set(value=0.035, min=0)
        pars_red["C"].set(30)
        pars_red["phi"].set(value=np.pi/2, min=0, max=2*np.pi)
        out_f_red = model_red.fit(y_red, x=x, params=pars_red)
        
        model_green = lmfit.model.Model(fitfun)
        pars_green = model_green.make_params()
        pars_green["A"].set(30)
        pars_green["B"].set(value=0.035, min=0)
        pars_green["C"].set(30)
        pars_green["phi"].set(value=np.pi/2, min=0, max=2*np.pi)
        out_f_green = model_green.fit(y_green, x=x, params=pars_green)
        
        model_blue = lmfit.model.Model(fitfun)
        pars_blue = model_blue.make_params()
        pars_blue["A"].set(30)
        pars_blue["B"].set(value=0.035, min=0)
        pars_blue["C"].set(30)
        pars_blue["phi"].set(value=np.pi/2, min=0, max=2*np.pi)
        out_f_blue = model_blue.fit(y_blue, x=x, params=pars_blue)
        
        # Store the parameter-values and -errors in a dictionary to store it to a file later on
        d_red = {}
        d_red["A_val"] = out_f_red.params["A"].value
        d_red["B_val"] = out_f_red.params["B"].value
        d_red["C_val"] = out_f_red.params["C"].value
        d_red["Phi_val"] = out_f_red.params["phi"].value
        d_red["A_err"] = out_f_red.params["A"].stderr
        d_red["B_err"] = out_f_red.params["B"].stderr
        d_red["C_err"] = out_f_red.params["C"].stderr
        d_red["Phi_err"] = out_f_red.params["phi"].stderr
        d_red[".Chi_red"] = out_f_red.redchi
        
        d_green = {}
        d_green["A_val"] = out_f_green.params["A"].value
        d_green["B_val"] = out_f_green.params["B"].value
        d_green["C_val"] = out_f_green.params["C"].value
        d_green["Phi_val"] = out_f_green.params["phi"].value
        d_green["A_err"] = out_f_green.params["A"].stderr
        d_green["B_err"] = out_f_green.params["B"].stderr
        d_green["C_err"] = out_f_green.params["C"].stderr
        d_green["Phi_err"] = out_f_green.params["phi"].stderr
        d_green[".Chi_red"] = out_f_green.redchi
        
        d_blue = {}
        d_blue["A_val"] = out_f_blue.params["A"].value
        d_blue["B_val"] = out_f_blue.params["B"].value
        d_blue["C_val"] = out_f_blue.params["C"].value
        d_blue["Phi_val"] = out_f_blue.params["phi"].value
        d_blue["A_err"] = out_f_blue.params["A"].stderr
        d_blue["B_err"] = out_f_blue.params["B"].stderr
        d_blue["C_err"] = out_f_blue.params["C"].stderr
        d_blue["Phi_err"] = out_f_blue.params["phi"].stderr
        d_blue[".Chi_red"] = out_f_blue.redchi
        
        # Append the dictionaries of each channel to one list that will be stored later on
        temp.append(d_red)
        temp.append(d_green)
        temp.append(d_blue)

        # Plot the data and the calculated fits for each channel
        fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
        fig.set_figheight(figsize_y)
        fig.set_figwidth(figsize_x)
        
        ax1.set_xlim(-10, 250)
        ax2.set_xlim(-10, 250)
        ax3.set_xlim(-10, 250)

        ax1.title.set_text(title + str(i+1))
        ax1.title.set_fontsize(20)

        ax1.errorbar(x, y_red, xerr=xerr, yerr=yerr, c="r", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Red")
        ax2.errorbar(x, y_green, xerr=xerr, yerr=yerr, c="g", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Green")
        ax3.errorbar(x, y_blue, xerr=xerr, yerr=yerr, c="b", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Blue")
        
        ax1.plot(np.arange(-5,175,0.01), out_f_red.eval(x=np.arange(-5,175,0.01)), '--k', lw=2, label = 'Fit - Channel Red')
        ax2.plot(np.arange(-5,175,0.01), out_f_green.eval(x=np.arange(-5,175,0.01)), '--k', lw=2, label = 'Fit - Channel Green')
        ax3.plot(np.arange(-5,175,0.01), out_f_blue.eval(x=np.arange(-5,175,0.01)), '--k', lw=2, label = 'Fit - Channel Blue')
        
        # Show the values and errors of the fit-parameters in each fit by displaying it in a textbox
        textstr = '\n'.join((
                    r'Mean = %.2f +- %.2f' % (d_red["C_val"], d_red["C_err"]),
                    r'Amp. = %.2f +- %.2f' % (d_red["A_val"], d_red["A_err"]),
                    r'Phase = %.2f +- %.2f' % (d_red["Phi_val"], d_red["Phi_err"]),
                    r'Stretch = %.4f +- %.4f' %(d_red["B_val"], d_red["B_err"]),
                    r'$\mathcal{X}^{\,2}_{red.}$ = %.2f' % (d_red[".Chi_red"], )))
        ax1.text(0.75, 0.90, textstr, transform=ax1.transAxes, fontsize=14,
                verticalalignment='top', bbox=props)
        
        textstr = '\n'.join((
                    r'Mean = %.2f +- %.2f' % (d_green["C_val"], d_green["C_err"]),
                    r'Amp. = %.2f +- %.2f' % (d_green["A_val"], d_green["A_err"]),
                    r'Phase = %.2f +- %.2f' % (d_green["Phi_val"], d_green["Phi_err"]),
                    r'Stretch = %.4f +- %.4f' %(d_green["B_val"], d_green["B_err"]),
                    r'$\mathcal{X}^{\,2}_{red.}$ = %.2f' % (d_green[".Chi_red"], )))
        ax2.text(0.75, 0.90, textstr, transform=ax2.transAxes, fontsize=14,
                verticalalignment='top', bbox=props)
        
        textstr = '\n'.join((
                    r'Mean = %.2f +- %.2f' % (d_blue["C_val"], d_blue["C_err"]),
                    r'Amp. = %.2f +- %.2f' % (d_blue["A_val"], d_blue["A_err"]),
                    r'Phase = %.2f +- %.2f' % (d_blue["Phi_val"], d_blue["Phi_err"]),
                    r'Stretch = %.4f +- %.4f' %(d_blue["B_val"], d_blue["B_err"]),
                    r'$\mathcal{X}^{\,2}_{red.}$ = %.2f' % (d_blue[".Chi_red"], )))
        ax3.text(0.75, 0.90, textstr, transform=ax3.transAxes, fontsize=14,
                verticalalignment='top', bbox=props)

        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14) 
        for tick in ax2.yaxis.get_major_ticks():
            tick.label.set_fontsize(14) 
        for tick in ax3.yaxis.get_major_ticks():
            tick.label.set_fontsize(14) 
        for tick in ax3.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)

        ax3.set_xlabel(xlabel, fontsize=18, labelpad=5)
        ax2.set_ylabel("Intensity (8 Bit)", fontsize=18, labelpad=10)

        # Save the plot
        plt.savefig(savepath_1 + str(i+1) + savepath_2, bbox_inches = 'tight', pad_inches = 0)
        plt.close()
        
        # Append the list containing the dictionaries to another list that will contain such dictionaries for each pixel
        # (1. dim: Pixel, 2. dim: Channel)
        l.append(temp)
        
    # Save the list of dictionaries
    data = np.array(l)
    np.save(savepath_dict, data)
    
# Method that fits the data stored at the given path and saves the fit-plot as well as the parameter-dictionary
# (Used for setups containing the waveplate, functionality is similar to "fitData", see above for more comments)
def fitData_WP(path, xlabel, title, savepath_1, savepath_2, savepath_3, savepath_dict):
    
    arr = np.load(path)
    
    l = []
    
    for i in range(0,10):
        
        temp_l = []
    
        # Loop over each position of the LP
        for j in range(0,len(steps_20)):
    
            temp = []

            x = arr[i,j,:,0]
            y_red = arr[i,j,:,1]
            y_green = arr[i,j,:,2]
            y_blue = arr[i,j,:,3]
            
            # Mask the data in the blue channel to neglect the last four data-points while fittings, since they always
            # seemed off (maybe there were some errors in measuring those points)
            x_blue_masked = x[mask_background_blue]
            x_blue_masked_inv = x[mask_background_blue_inv]
            y_blue_masked = y_blue[mask_background_blue]
            y_blue_masked_inv = y_blue[mask_background_blue_inv]

            # Fit the data for each channel step by step, since fitting it directly in one step did not work for many 
            # data-sets, therefore only fit the amplitude A and the mean C in the first step, keeping the stretch B and the
            # phase phi fixed. 
            # Then use the results of this fit as starting values for A and C and keep them fixed while fitting B and phi.
            # In the end use the fit-results for all parameters as starting values and fit them all together
            model_red = lmfit.model.Model(fitfun)
            pars_red = model_red.make_params()
            pars_red["A"].set(value=4)
            pars_red["B"].set(value=0.043, min=0.0, max=0.1, vary=False)
            pars_red["C"].set(value=np.mean(y_red), min=np.mean(y_red)-30)
            pars_red["phi"].set(value=((3.3+j*0.7) % (2*np.pi)), min=0, max=2*np.pi, vary=False)
            out_f_red = model_red.fit(y_red, x=x, params=pars_red)
            
            pars_red["A"].set(value=out_f_red.params["A"].value, vary=False)
            pars_red["B"].set(value=0.043, min=0.0, max=0.1, vary=True)
            pars_red["C"].set(value=out_f_red.params["C"].value, vary=False)
            pars_red["phi"].set(value=((3.3+j*0.7) % (2*np.pi)), min=0, max=2*np.pi, vary=True)
            out_f_red = model_red.fit(y_red, x=x, params=pars_red)
            
            pars_red["A"].set(value=out_f_red.params["A"].value, vary=True)
            pars_red["B"].set(value=out_f_red.params["B"].value, vary=True)
            pars_red["C"].set(value=out_f_red.params["C"].value, vary=True)
            pars_red["phi"].set(value=out_f_red.params["phi"].value, vary=True)
            out_f_red = model_red.fit(y_red, x=x, params=pars_red)
            

            model_green = lmfit.model.Model(fitfun)
            pars_green = model_green.make_params()
            pars_green["A"].set(value=15)
            pars_green["B"].set(value=0.034, min=0.028, max=0.06, vary=False)
            pars_green["C"].set(value=np.mean(y_green), min=np.mean(y_green)-30)
            pars_green["phi"].set(value=((1.2+j*0.7) % (2*np.pi)), min=0, max=2*np.pi, vary=False)
            out_f_green = model_green.fit(y_green, x=x, params=pars_green)
            
            pars_green["A"].set(value=out_f_green.params["A"].value, vary=False)
            pars_green["B"].set(value=0.034, min=0.028, max=0.06, vary=True)
            pars_green["C"].set(value=out_f_green.params["C"].value, vary=False)
            pars_green["phi"].set(value=((1.2+j*0.7) % (2*np.pi)), min=0, max=2*np.pi, vary=True)
            out_f_green = model_green.fit(y_green, x=x, params=pars_green)
            
            pars_green["A"].set(value=out_f_green.params["A"].value, vary=True)
            pars_green["B"].set(value=out_f_green.params["B"].value, vary=True)
            pars_green["C"].set(value=out_f_green.params["C"].value, vary=True)
            pars_green["phi"].set(value=out_f_green.params["phi"].value, vary=True)
            out_f_green = model_green.fit(y_green, x=x, params=pars_green)

            
            model_blue = lmfit.model.Model(fitfun)
            pars_blue = model_blue.make_params()
            pars_blue["A"].set(value=45)
            pars_blue["B"].set(value=0.048, min=0.035, max=0.06, vary=False)
            pars_blue["C"].set(value=np.mean(y_blue), min=np.mean(y_blue)-30)
            pars_blue["phi"].set(value=((1.3+j*0.7) % (2*np.pi)), min=0, max=2*np.pi, vary=False)
            out_f_blue = model_blue.fit(y_blue_masked, x=x_blue_masked, params=pars_blue)
            #out_f_blue = model_blue.fit(y_blue, x=x, params=pars_blue)
            
            pars_blue["A"].set(value=out_f_blue.params["A"].value, vary=False)
            pars_blue["B"].set(value=0.048, min=0.035, max=0.06, vary=True)
            pars_blue["C"].set(value=out_f_blue.params["C"].value, vary=False)
            pars_blue["phi"].set(value=((1.3+j*0.7) % (2*np.pi)), min=0, max=2*np.pi, vary=True)
            out_f_blue = model_blue.fit(y_blue_masked, x=x_blue_masked, params=pars_blue)
            #out_f_blue = model_blue.fit(y_blue, x=x, params=pars_blue)
            
            pars_blue["A"].set(value=out_f_blue.params["A"].value, vary=True)
            pars_blue["B"].set(value=out_f_blue.params["B"].value, vary=True)
            pars_blue["C"].set(value=out_f_blue.params["C"].value, vary=True)
            pars_blue["phi"].set(value=out_f_blue.params["phi"].value, vary=True)
            out_f_blue = model_blue.fit(y_blue_masked, x=x_blue_masked, params=pars_blue)
            #out_f_blue = model_blue.fit(y_blue, x=x, params=pars_blue)

            d_red = {}
            d_red["A_val"] = out_f_red.params["A"].value
            d_red["B_val"] = out_f_red.params["B"].value
            d_red["C_val"] = out_f_red.params["C"].value
            d_red["Phi_val"] = out_f_red.params["phi"].value
            d_red["A_err"] = out_f_red.params["A"].stderr
            d_red["B_err"] = out_f_red.params["B"].stderr
            d_red["C_err"] = out_f_red.params["C"].stderr
            d_red["Phi_err"] = out_f_red.params["phi"].stderr
            d_red[".Chi_red"] = out_f_red.redchi

            d_green = {}
            d_green["A_val"] = out_f_green.params["A"].value
            d_green["B_val"] = out_f_green.params["B"].value
            d_green["C_val"] = out_f_green.params["C"].value
            d_green["Phi_val"] = out_f_green.params["phi"].value
            d_green["A_err"] = out_f_green.params["A"].stderr
            d_green["B_err"] = out_f_green.params["B"].stderr
            d_green["C_err"] = out_f_green.params["C"].stderr
            d_green["Phi_err"] = out_f_green.params["phi"].stderr
            d_green[".Chi_red"] = out_f_green.redchi

            d_blue = {}
            d_blue["A_val"] = out_f_blue.params["A"].value
            d_blue["B_val"] = out_f_blue.params["B"].value
            d_blue["C_val"] = out_f_blue.params["C"].value
            d_blue["Phi_val"] = out_f_blue.params["phi"].value
            d_blue["A_err"] = out_f_blue.params["A"].stderr
            d_blue["B_err"] = out_f_blue.params["B"].stderr
            d_blue["C_err"] = out_f_blue.params["C"].stderr
            d_blue["Phi_err"] = out_f_blue.params["phi"].stderr
            d_blue[".Chi_red"] = out_f_blue.redchi

            temp.append(d_red)
            temp.append(d_green)
            temp.append(d_blue)


            fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
            fig.set_figheight(figsize_y)
            fig.set_figwidth(figsize_x)

            ax1.set_xlim(-10, 250)
            ax2.set_xlim(-10, 250)
            ax3.set_xlim(-10, 250)

            ax1.title.set_text(title + str(i+1) + " - LP: "+str(steps_20[j]) + "°")
            ax1.title.set_fontsize(20)

            ax1.errorbar(x, y_red, xerr=xerr, yerr=yerr, c="r", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Red")
            ax2.errorbar(x, y_green, xerr=xerr, yerr=yerr, c="g", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Green")
            # Plot the data used for fitting in dark blue and the data neglected in fitting in cyan
            ax3.errorbar(x_blue_masked, y_blue_masked, xerr=xerr, yerr=yerr, c="b", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Blue")
            ax3.errorbar(x_blue_masked_inv, y_blue_masked_inv, xerr=xerr, yerr=yerr, c="cyan", ls = "", marker="o")
            #ax3.errorbar(x, y_blue, xerr=xerr, yerr=yerr, c="b", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Blue")
            
            ax1.plot(np.arange(-5,175,0.01), out_f_red.eval(x=np.arange(-5,175,0.01)), '--k', lw=2, label = 'Fit - Channel Red')
            ax2.plot(np.arange(-5,175,0.01), out_f_green.eval(x=np.arange(-5,175,0.01)), '--k', lw=2, label = 'Fit - Channel Green')
            ax3.plot(np.arange(-5,175,0.01), out_f_blue.eval(x=np.arange(-5,175,0.01)), '--k', lw=2, label = 'Fit - Channel Blue')

            textstr = '\n'.join((
                        r'Mean = %.2f +- %.2f' % (d_red["C_val"], d_red["C_err"]),
                        r'Amp. = %.2f +- %.2f' % (d_red["A_val"], d_red["A_err"]),
                        r'Phase = %.2f +- %.2f' % (d_red["Phi_val"], d_red["Phi_err"]),
                        r'Stretch = %.4f +- %.4f' %(d_red["B_val"], d_red["B_err"]),
                        r'$\mathcal{X}^{\,2}_{red.}$ = %.2f' % (d_red[".Chi_red"], )))
            ax1.text(0.75, 0.90, textstr, transform=ax1.transAxes, fontsize=14,
                    verticalalignment='top', bbox=props)

            textstr = '\n'.join((
                        r'Mean = %.2f +- %.2f' % (d_green["C_val"], d_green["C_err"]),
                        r'Amp. = %.2f +- %.2f' % (d_green["A_val"], d_green["A_err"]),
                        r'Phase = %.2f +- %.2f' % (d_green["Phi_val"], d_green["Phi_err"]),
                        r'Stretch = %.4f +- %.4f' %(d_green["B_val"], d_green["B_err"]),
                        r'$\mathcal{X}^{\,2}_{red.}$ = %.2f' % (d_green[".Chi_red"], )))
            ax2.text(0.75, 0.90, textstr, transform=ax2.transAxes, fontsize=14,
                    verticalalignment='top', bbox=props)

            textstr = '\n'.join((
                        r'Mean = %.2f +- %.2f' % (d_blue["C_val"], d_blue["C_err"]),
                        r'Amp. = %.2f +- %.2f' % (d_blue["A_val"], d_blue["A_err"]),
                        r'Phase = %.2f +- %.2f' % (d_blue["Phi_val"], d_blue["Phi_err"]),
                        r'Stretch = %.4f +- %.4f' %(d_blue["B_val"], d_blue["B_err"]),
                        r'$\mathcal{X}^{\,2}_{red.}$ = %.2f' % (d_blue[".Chi_red"], )))
            ax3.text(0.75, 0.90, textstr, transform=ax3.transAxes, fontsize=14,
                    verticalalignment='top', bbox=props)

            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax2.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax3.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax3.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)

            ax3.set_xlabel(xlabel, fontsize=18, labelpad=5)
            ax2.set_ylabel("Intensity (8 Bit)", fontsize=18, labelpad=10)

            plt.savefig(savepath_1 + str(i+1) + savepath_2 +str(i+1) + savepath_3 + str(steps_20[j])+".png", bbox_inches = 'tight', pad_inches = 0)
            plt.close()
            
            temp_l.append(temp)
        
        l.append(temp_l)
        
    data = np.array(l)
    np.save(savepath_dict, data)
    

# Method that fits the data stored at the given path and saves the fit-plot as well as the parameter-dictionary
# (Used for setups containing the Quarterwave-retarder, functionality is similar to "fitData", see above for more comments)
def fitData_L4(path, xlabel, title, savepath_1, savepath_2, savepath_3, savepath_dict):
    
    arr = np.load(path)
    
    l = []
    
    for i in range(0,10):
        
        temp_l = []
    
        for j in range(0,len(steps_45)):
    
            temp = []

            x = arr[i,j,:,0]
            y_red = arr[i,j,:,1]
            y_green = arr[i,j,:,2]
            y_blue = arr[i,j,:,3]

            model_red = lmfit.model.Model(fitfun)
            pars_red = model_red.make_params()
            pars_red["A"].set(value=1, min=0, max=50)
            pars_red["B"].set(value=0.035, min=0.00, max=0.1, vary=False)
            pars_red["C"].set(value=np.mean(y_red), min=np.mean(y_red)-30)
            pars_red["phi"].set(value=((2.6+j*1.8) % (2*np.pi)), min=0, max=2*np.pi, vary=False)
            out_f_red = model_red.fit(y_red, x=x, params=pars_red)
            
            pars_red["A"].set(value=out_f_red.params["A"].value, vary=False)
            pars_red["B"].set(value=0.035, min=0.00, max=0.1, vary=True)
            pars_red["C"].set(value=out_f_red.params["C"].value, vary=False)
            pars_red["phi"].set(value=((2.6+j*1.8) % (2*np.pi)), min=0, max=2*np.pi, vary=True)
            out_f_red = model_red.fit(y_red, x=x, params=pars_red)
            
            pars_red["A"].set(value=out_f_red.params["A"].value, min=0, max=50, vary=True)
            pars_red["B"].set(value=out_f_red.params["B"].value, min=0.00, max=0.1, vary=True)
            pars_red["C"].set(value=out_f_red.params["C"].value, min=np.mean(y_red)-30, vary=True)
            pars_red["phi"].set(value=out_f_red.params["phi"].value, min=0, max=2*np.pi, vary=True)
            out_f_red = model_red.fit(y_red, x=x, params=pars_red)
            

            model_green = lmfit.model.Model(fitfun)
            pars_green = model_green.make_params()
            pars_green["A"].set(value=1, min=0, max=50)
            pars_green["B"].set(value=0.035, min=0.00, max=0.1, vary=False)
            pars_green["C"].set(value=np.mean(y_green), min=np.mean(y_green)-30)
            pars_green["phi"].set(value=((3.0+j*1.6) % (2*np.pi)), min=0, max=2*np.pi, vary=False)
            out_f_green = model_green.fit(y_green, x=x, params=pars_green)
            
            pars_green["A"].set(value=out_f_green.params["A"].value, vary=False)
            pars_green["B"].set(value=0.035, min=0.00, max=0.1, vary=True)
            pars_green["C"].set(value=out_f_green.params["C"].value, vary=False)
            pars_green["phi"].set(value=((3.0+j*1.6) % (2*np.pi)), min=0, max=2*np.pi, vary=True)
            out_f_green = model_green.fit(y_green, x=x, params=pars_green)
            
            pars_green["A"].set(value=out_f_green.params["A"].value, min=0, max=50, vary=True)
            pars_green["B"].set(value=out_f_green.params["B"].value, min=0.00, max=0.1, vary=True)
            pars_green["C"].set(value=out_f_green.params["C"].value, vary=True)
            pars_green["phi"].set(value=out_f_green.params["phi"].value, min=0, max=2*np.pi, vary=True)
            out_f_green = model_green.fit(y_green, x=x, params=pars_green)

            
            model_blue = lmfit.model.Model(fitfun)
            pars_blue = model_blue.make_params()
            pars_blue["A"].set(value=1, min=0, max=100)
            pars_blue["B"].set(value=0.035, min=0.00, max=0.1, vary=False)
            pars_blue["C"].set(value=np.mean(y_blue), min=np.mean(y_blue)-30)
            pars_blue["phi"].set(value=((3.6+j*1.3) % (2*np.pi)), min=0, max=2*np.pi, vary=False)
            out_f_blue = model_blue.fit(y_blue, x=x, params=pars_blue)
            
            pars_blue["A"].set(value=out_f_blue.params["A"].value, vary=False)
            pars_blue["B"].set(value=0.035, min=0.00, max=0.1, vary=True)
            pars_blue["C"].set(value=out_f_blue.params["C"].value, vary=False)
            pars_blue["phi"].set(value=((3.6+j*1.3) % (2*np.pi)), min=0, max=2*np.pi, vary=True)
            out_f_blue = model_blue.fit(y_blue, x=x, params=pars_blue)
            
            pars_blue["A"].set(value=out_f_blue.params["A"].value, min=0, max=100, vary=True)
            pars_blue["B"].set(value=out_f_blue.params["B"].value, min=0.00, max=0.1, vary=True)
            pars_blue["C"].set(value=out_f_blue.params["C"].value, vary=True)
            pars_blue["phi"].set(value=out_f_blue.params["phi"].value, min=0, max=2*np.pi, vary=True)
            out_f_blue = model_blue.fit(y_blue, x=x, params=pars_blue)

            d_red = {}
            d_red["A_val"] = out_f_red.params["A"].value
            d_red["B_val"] = out_f_red.params["B"].value
            d_red["C_val"] = out_f_red.params["C"].value
            d_red["Phi_val"] = out_f_red.params["phi"].value
            d_red["A_err"] = out_f_red.params["A"].stderr
            d_red["B_err"] = out_f_red.params["B"].stderr
            d_red["C_err"] = out_f_red.params["C"].stderr
            d_red["Phi_err"] = out_f_red.params["phi"].stderr
            d_red[".Chi_red"] = out_f_red.redchi

            d_green = {}
            d_green["A_val"] = out_f_green.params["A"].value
            d_green["B_val"] = out_f_green.params["B"].value
            d_green["C_val"] = out_f_green.params["C"].value
            d_green["Phi_val"] = out_f_green.params["phi"].value
            d_green["A_err"] = out_f_green.params["A"].stderr
            d_green["B_err"] = out_f_green.params["B"].stderr
            d_green["C_err"] = out_f_green.params["C"].stderr
            d_green["Phi_err"] = out_f_green.params["phi"].stderr
            d_green[".Chi_red"] = out_f_green.redchi

            d_blue = {}
            d_blue["A_val"] = out_f_blue.params["A"].value
            d_blue["B_val"] = out_f_blue.params["B"].value
            d_blue["C_val"] = out_f_blue.params["C"].value
            d_blue["Phi_val"] = out_f_blue.params["phi"].value
            d_blue["A_err"] = out_f_blue.params["A"].stderr
            d_blue["B_err"] = out_f_blue.params["B"].stderr
            d_blue["C_err"] = out_f_blue.params["C"].stderr
            d_blue["Phi_err"] = out_f_blue.params["phi"].stderr
            d_blue[".Chi_red"] = out_f_blue.redchi

            temp.append(d_red)
            temp.append(d_green)
            temp.append(d_blue)


            fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
            fig.set_figheight(figsize_y)
            fig.set_figwidth(figsize_x)

            ax1.set_xlim(-10, 250)
            ax2.set_xlim(-10, 250)
            ax3.set_xlim(-10, 250)

            ax1.title.set_text(title + str(i+1) + " - L4: "+str(steps_45[j]) + "°")
            ax1.title.set_fontsize(20)

            ax1.errorbar(x, y_red, xerr=xerr, yerr=yerr, c="r", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Red")
            ax2.errorbar(x, y_green, xerr=xerr, yerr=yerr, c="g", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Green")
            ax3.errorbar(x, y_blue, xerr=xerr, yerr=yerr, c="b", ls = "", marker="o", label="Pixel " + str(i+1) + " - Channel Blue")

            ax1.plot(np.arange(-5,175,0.01), out_f_red.eval(x=np.arange(-5,175,0.01)), '--k', lw=2, label = 'Fit - Channel Red')
            ax2.plot(np.arange(-5,175,0.01), out_f_green.eval(x=np.arange(-5,175,0.01)), '--k', lw=2, label = 'Fit - Channel Green')
            ax3.plot(np.arange(-5,175,0.01), out_f_blue.eval(x=np.arange(-5,175,0.01)), '--k', lw=2, label = 'Fit - Channel Blue')

            textstr = '\n'.join((
                        r'Mean = %.2f +- %.2f' % (d_red["C_val"], d_red["C_err"]),
                        r'Amp. = %.2f +- %.2f' % (d_red["A_val"], d_red["A_err"]),
                        r'Phase = %.2f +- %.2f' % (d_red["Phi_val"], d_red["Phi_err"]),
                        r'Stretch = %.4f +- %.4f' %(d_red["B_val"], d_red["B_err"]),
                        r'$\mathcal{X}^{\,2}_{red.}$ = %.2f' % (d_red[".Chi_red"], )))
            ax1.text(0.75, 0.90, textstr, transform=ax1.transAxes, fontsize=14,
                    verticalalignment='top', bbox=props)

            textstr = '\n'.join((
                        r'Mean = %.2f +- %.2f' % (d_green["C_val"], d_green["C_err"]),
                        r'Amp. = %.2f +- %.2f' % (d_green["A_val"], d_green["A_err"]),
                        r'Phase = %.2f +- %.2f' % (d_green["Phi_val"], d_green["Phi_err"]),
                        r'Stretch = %.4f +- %.4f' %(d_green["B_val"], d_green["B_err"]),
                        r'$\mathcal{X}^{\,2}_{red.}$ = %.2f' % (d_green[".Chi_red"], )))
            ax2.text(0.75, 0.90, textstr, transform=ax2.transAxes, fontsize=14,
                    verticalalignment='top', bbox=props)

            textstr = '\n'.join((
                        r'Mean = %.2f +- %.2f' % (d_blue["C_val"], d_blue["C_err"]),
                        r'Amp. = %.2f +- %.2f' % (d_blue["A_val"], d_blue["A_err"]),
                        r'Phase = %.2f +- %.2f' % (d_blue["Phi_val"], d_blue["Phi_err"]),
                        r'Stretch = %.4f +- %.4f' %(d_blue["B_val"], d_blue["B_err"]),
                        r'$\mathcal{X}^{\,2}_{red.}$ = %.2f' % (d_blue[".Chi_red"], )))
            ax3.text(0.75, 0.90, textstr, transform=ax3.transAxes, fontsize=14,
                    verticalalignment='top', bbox=props)

            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax2.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax3.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax3.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)

            ax3.set_xlabel(xlabel, fontsize=18, labelpad=5)
            ax2.set_ylabel("Intensity (8 Bit)", fontsize=18, labelpad=10)

            plt.savefig(savepath_1 + str(i+1) + savepath_2 +str(i+1) + savepath_3 + str(steps_45[j])+".png", bbox_inches = 'tight', pad_inches = 0)
            plt.close()
            
            temp_l.append(temp)
        
        l.append(temp_l)
        
    data = np.array(l)
    np.save(savepath_dict, data)
    

# ---------------------------------------- #
# Main Program
# ---------------------------------------- #
	
# Background

file = "PixelData_CP_LP"
path = directory + file + appendix
fitData(path, "Angle of Linear Polarizer [°]", "Sinusoidal Fit - Background - CP_LP - Pixel_","Fitting\Images\Background\CP_LP\Pixel_", "-CP_LP.png", "Fitting\Data\Background\CP_LP.npy")

file = "PixelData_LP_LP"
path = directory + file + appendix
fitData(path, "Angle of Linear Polarizer [°]", "Sinusoidal Fit - Background - LP_LP - Pixel_","Fitting\Images\Background\LP_LP\Pixel_", "-LP_LP.png", "Fitting\Data\Background\LP_LP.npy")

file = "PixelData_CP_WP_LP"
path = directory + file + appendix
fitData_WP(path, "Angle of Waveplate [°]", "Sinusoidal Fit - Background - CP_WP_LP - Pixel_","Fitting\Images\Background\CP_WP_LP\Pixel_", "/pixel_", "-CP_WP_LP_", "Fitting\Data\Background\CP_WP_LP.npy")

file = "PixelData_LP_WP_LP"
path = directory + file + appendix
fitData_WP(path, "Angle of Waveplate [°]", "Sinusoidal Fit - Background - LP_WP_LP - Pixel_","Fitting\Images\Background\LP_WP_LP\Pixel_", "/pixel_", "-LP_WP_LP_", "Fitting\Data\Background\LP_WP_LP.npy")

file = "PixelData_CP_L4_LP"
path = directory + file + appendix
fitData_L4(path, "Angle of Linear Polarizer [°]", "Sinusoidal Fit - Background - CP_L4_LP - Pixel_","Fitting\Images\Background\CP_L4_LP\Pixel_", "/pixel_", "-CP_L4_LP_", "Fitting\Data\Background\CP_L4_LP.npy")


# Sample

file = "PixelData_CP_LP"
path = directory_S + file + appendix
fitData(path, "Angle of Linear Polarizer [°]", "Sinusoidal Fit - Sample - CP_LP - Pixel_","Fitting\Images\Sample\CP_LP\Pixel_", "-CP_LP.png", "Fitting\Data\Sample\CP_LP.npy")

file = "PixelData_LP_LP"
path = directory_S + file + appendix
fitData(path, "Angle of Linear Polarizer [°]", "Sinusoidal Fit - Sample - LP_LP - Pixel_","Fitting\Images\Sample\LP_LP\Pixel_", "-LP_LP", "Fitting\Data\Sample\LP_LP.npy")

file = "PixelData_CP_WP_LP"
path = directory_S + file + appendix
fitData_WP(path, "Angle of Waveplate [°]", "Sinusoidal Fit - Sample - CP_WP_LP - Pixel_","Fitting\Images\Sample\CP_WP_LP\Pixel_", "/pixel_", "-CP_WP_LP_", "Fitting\Data\Sample\CP_WP_LP.npy")

file = "PixelData_LP_WP_LP"
path = directory_S + file + appendix
fitData_WP(path, "Angle of Waveplate [°]", "Sinusoidal Fit - Sample - LP_WP_LP - Pixel_","Fitting\Images\Sample\LP_WP_LP\Pixel_", "/pixel_", "-LP_WP_LP_", "Fitting\Data\Sample\LP_WP_LP.npy")

file = "PixelData_CP_L4_LP"
path = directory_S + file + appendix
fitData_L4(path, "Angle of Linear Polarizer [°]", "Sinusoidal Fit - Sample - CP_L4_LP - Pixel_","Fitting\Images\Sample\CP_L4_LP\Pixel_", "/pixel_", "-CP_L4_LP_", "Fitting\Data\Sample\CP_L4_LP.npy")


# Difference

file = "PixelData_CP_LP"
path = directory_D + file + appendix
fitData(path, "Angle of Linear Polarizer [°]", "Sinusoidal Fit - Difference - CP_LP - Pixel_","Fitting\Images\Difference\CP_LP\Pixel_", "-CP_LP.png", "Fitting\Data\Difference\CP_LP.npy")

file = "PixelData_LP_LP"
path = directory_D + file + appendix
fitData(path, "Angle of Linear Polarizer [°]", "Sinusoidal Fit - Difference - LP_LP - Pixel_","Fitting\Images\Difference\LP_LP\Pixel_", "-LP_LP", "Fitting\Data\Difference\LP_LP.npy")

file = "PixelData_CP_WP_LP"
path = directory_D + file + appendix
fitData_WP(path, "Angle of Waveplate [°]", "Sinusoidal Fit - Difference - CP_WP_LP - Pixel_","Fitting\Images\Difference\CP_WP_LP\Pixel_", "/pixel_", "-CP_WP_LP_", "Fitting\Data\Difference\CP_WP_LP.npy")

file = "PixelData_LP_WP_LP"
path = directory_D + file + appendix
fitData_WP(path, "Angle of Waveplate [°]", "Sinusoidal Fit - Difference - LP_WP_LP - Pixel_","Fitting\Images\Difference\LP_WP_LP\Pixel_", "/pixel_", "-LP_WP_LP_", "Fitting\Data\Difference\LP_WP_LP.npy")

file = "PixelData_CP_L4_LP"
path = directory_D + file + appendix
fitData_L4(path, "Angle of Linear Polarizer [°]", "Sinusoidal Fit - Difference - CP_L4_LP - Pixel_","Fitting\Images\Difference\CP_L4_LP\Pixel_", "/pixel_", "-CP_L4_LP_", "Fitting\Data\Difference\CP_L4_LP.npy")

