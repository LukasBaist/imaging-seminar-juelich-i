# ---------------------------------------- #
# Imports
# ---------------------------------------- #

from PIL import Image

import numpy as np
import lmfit
import matplotlib.pyplot as plt


# ---------------------------------------- #
# Setup
# ---------------------------------------- #

directory = "../Measurements/Lab/"
directory_shift = "../Measurements/Lab_shifted/"
appendix = ".tiff"

pixels = []
pixels.append([1 ,  324,  972])
pixels.append([2 , 1215,   63])
pixels.append([3 , 1283,  295])
pixels.append([4 , 2889, 1090])
pixels.append([5 , 3027, 2302])
pixels.append([6 , 2973, 2379])
pixels.append([7 ,  693, 1242])
pixels.append([8 , 2133, 1325])
pixels.append([9 , 1675,  691])
pixels.append([10, 1477, 1420])

figsize_x = 12
figsize_y = 12

steps_20 = range(10,180,20)
steps_10 = range(0,180,10)
steps_40 = [0, 40, 90, 130]
steps_45 = range(0,180,45)
steps_45_2 = range(0,135,45)
steps_90 = range(0,180,90)

channels = [0,1,2]

get_ipython().magic('matplotlib inline')


# ---------------------------------------- #
# Functions
# ---------------------------------------- #

# Function that subtracts two images, keeping the alpha at 255
def subtract(a, b):
    n = a.shape[0]
    m = a.shape[1]    
    res = np.zeros([n,m,4], dtype="ubyte")
    for i in range(0,n):
        for j in range(0,m):
            res[i][j] = (a[i][j] - b[i][j])
            res[i][j][3] = 255            
    return res


# Function that plots the positions of the selected pixels on the given image
def showPixels(image, pixels, savename):
    plt.figure(figsize=(figsize_x, figsize_y), dpi=200)
    plt.imshow(image)
    for i in range(len(pixels)):
        plt.plot(pixels[i][1], pixels[i][2], marker="+", markersize=8, c="b")
        plt.annotate(str(pixels[i][0]), xy=(pixels[i][1], pixels[i][2]), xytext=(pixels[i][1]+40, pixels[i][2]+30), fontsize=20)        
    plt.title("Pixel Positions", fontsize=24)
    plt.savefig(savename, bbox_inches = 'tight', pad_inches = 0, dpi=200)
    plt.show()
    
    
# Function that plots the position of a certain selected pixel on the given image with a given padding
def showPixelsDetailed(image, pixels, savename, pixel, pad):
    plt.figure(figsize=(figsize_x/4, figsize_y/4), dpi=100)
    plt.imshow(image)
    plt.plot(pixels[pixel-1][1], pixels[pixel-1][2], marker="+", markersize=14, c="b")
    plt.title("Pixel "+str(pixel), fontsize=20)
    plt.xlim(pixels[pixel-1][1]-pad, pixels[pixel-1][1]+pad)
    plt.ylim(pixels[pixel-1][2]+pad, pixels[pixel-1][2]-pad)
    plt.savefig(savename, bbox_inches = 'tight', pad_inches = 0, dpi=100)
    plt.show()

    
# Function that returns the intensity for each pixel for each image in the given path for the given channels
def getIntensityGraph_LP(path, range_lp, pixels, channels):
    a = len(range_lp)
    c = len(pixels)
    d = len(channels)
    
    res = np.empty([c,a,d+1])
                    
    for l in range(0, a):
        img = Image.open(path + "img_" + str(range_lp[l]) + appendix)
        im = np.array(img)
        for j in range(0, c):
            res[j, l, 0] = range_lp[l]
            for k in range(0, d):
                res[j, l, k+1] = im[pixels[j][2]][pixels[j][1]][channels[k]]
        print(str(l+1) + " / " + str(len(range_lp)))
    return res    
  
    
# Function that returns the intensity for each pixel for each image in the given path for the given channels
# (Used for a setup containing a LP and a WP)
def getIntensityGraph_LP_WP(path, range_lp, range_wp, pixels, channels):
    a = len(range_lp)
    b = len(range_wp)
    c = len(pixels)
    d = len(channels)
    
    res = np.empty([c,a,b,d+1])
                    
    for l in range(0, a):
        for i in range(0, b):
            img = Image.open(path + "img_lp_" + str(range_lp[l]) + "_wp_" + str(range_wp[i]) + appendix)
            im = np.array(img)
            for j in range(0, c):
                res[j, l, i, 0] = range_wp[i]
                for k in range(0, d):
                    res[j, l, i, k+1] = im[pixels[j][2]][pixels[j][1]][channels[k]]
        print(str(l+1) + " / " + str(len(range_lp)))
    return res


# Function that returns the intensity for each pixel for each image in the given path with for the given channels
# (Used for a setup containing a LP and a Quarterwave-retarder)
def getIntensityGraph_L4_LP(path, range_l4, range_lp, pixels, channels):
    a = len(range_l4)
    b = len(range_lp)
    c = len(pixels)
    d = len(channels)
    
    res = np.empty([c,a,b,d+1])
                    
    for l in range(0, a):
        for i in range(0, b):
            img = Image.open(path + "img_L4_" + str(range_l4[l]) + "_lp_" + str(range_lp[i]) + appendix)
            im = np.array(img)
            for j in range(0, c):
                res[j, l, i, 0] = range_lp[i]
                for k in range(0, d):
                    res[j, l, i, k+1] = im[pixels[j][2]][pixels[j][1]][channels[k]]
        print(str(l+1) + " / " + str(len(range_l4)))
    return res


# Function that returns the intensity for each pixel for each image in the given path for the given channels
# (Used for a setup containing a LP, a Quarterwave-retarder and the sample)
def getIntensityGraph_L4_LP_sample(path, range_l4, range_lp, pixels, channels):
    a = len(range_l4)
    b = len(range_lp)
    c = len(pixels)
    d = len(channels)
    
    res = np.empty([c,a,b,d+1])
                    
    for l in range(0, a):
        for i in range(0, b):
            img = Image.open(path + "img_lp_" + str(range_lp[i]) + "_L4_" + str(range_l4[l]) + appendix)
            im = np.array(img)
            for j in range(0, c):
                res[j, l, i, 0] = range_lp[i]
                for k in range(0, d):
                    res[j, l, i, k+1] = im[pixels[j][2]][pixels[j][1]][channels[k]]
        print(str(l+1) + " / " + str(len(range_l4)))
    return res


# Function that returns the intensity for each pixel for each image in the given path for the given channels
# (Used for a setup containing a LP, a Quarterwave-retarder and a waveplate)
def getIntensityGraph_L4_WP_LP(path, range_l4, range_wp, range_lp, pixels, channels):
    a = len(range_l4)
    b = len(range_wp)
    c = len(pixels)
    d = len(channels)
    e = len(range_lp)
    
    res = np.empty([e,c,a,b,d+1])
    
    for q in range(0, e):
        for l in range(0, a):
            for i in range(0, b):
                img = Image.open(path + "img_lp_" + str(range_lp[q]) + "_L4_" + str(range_l4[l]) + "_wp_" + str(range_wp[i]) + appendix)
                im = np.array(img)
                for j in range(0, c):
                    res[q, j, l, i, 0] = range_wp[i]
                    for k in range(0, d):
                        res[q, j, l, i, k+1] = im[pixels[j][2]][pixels[j][1]][channels[k]]
            print(str(l+1) + " / " + str(len(range_l4)))
    return res


# Function that displays the intensity graph for the given pixel for each channel and saves it as an image
# (Used for a setup containing a LP)
def plotIntensity_LP(arr, pixels, title, xlabel, savepath_1, savepath_2):
    for i in range(0,len(pixels)):
        fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
        fig.set_figheight(figsize_y)
        fig.set_figwidth(figsize_x)

        ax1.title.set_text(title + str(pixels[i][0]))
        ax1.title.set_fontsize(20)

        ax1.plot(arr[i,:,0], arr[i,:,1], c="r", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[0]+1))
        ax2.plot(arr[i,:,0], arr[i,:,2], c="g", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[1]+1))
        ax3.plot(arr[i,:,0], arr[i,:,3], c="b", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[2]+1))

        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14) 
        for tick in ax2.yaxis.get_major_ticks():
            tick.label.set_fontsize(14) 
        for tick in ax3.yaxis.get_major_ticks():
            tick.label.set_fontsize(14) 
        for tick in ax3.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)

        ax3.set_xlabel(xlabel, fontsize=18, labelpad=5)
        ax2.set_ylabel("Intensity (8 Bit)", fontsize=18, labelpad=10)

        plt.savefig(savepath_1 + str(pixels[i][0]) + savepath_2, bbox_inches = 'tight', pad_inches = 0)
        plt.close()

# Function that displays the intensity graph for the given pixel for each channel and saves it as an image
# (Used for a setup containing a LP and a waveplate)
def plotIntensity_WP_LP(arr, pixels, title, xlabel, savepath_1, savepath_2, savepath_3):
    for i in range(0,len(pixels)):
        for j in range(0,len(steps_20)):
            fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
            fig.set_figheight(figsize_y)
            fig.set_figwidth(figsize_x)

            ax1.title.set_text(title + str(pixels[i][0]) + " - LP: "+str(steps_20[j]) + "°")
            ax1.title.set_fontsize(20)

            ax1.plot(arr[i,j,:,0], arr[i,j,:,1], c="r", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[0]+1))
            ax2.plot(arr[i,j,:,0], arr[i,j,:,2], c="g", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[1]+1))
            ax3.plot(arr[i,j,:,0], arr[i,j,:,3], c="b", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[2]+1))

            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax2.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax3.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax3.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)

            ax3.set_xlabel(xlabel, fontsize=18, labelpad=5)
            ax2.set_ylabel("Intensity (8 Bit)", fontsize=18, labelpad=10)

            plt.savefig(savepath_1 + str(pixels[i][0]) + savepath_2 +str(pixels[i][0]) + savepath_3 + str(steps_20[j])+".png", bbox_inches = 'tight', pad_inches = 0)
            plt.close()
            
# Function that displays the intensity graph for the given pixel for each channel and saves it as an image
# (Used for a setup containing a LP and a Quarterwave-retarder)            
def plotIntensity_L4_LP(arr, pixels, title, xlabel, savepath_1, savepath_2, savepath_3):
    for i in range(0,len(pixels)):
        for j in range(0,len(steps_45)):
            fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
            fig.set_figheight(figsize_y)
            fig.set_figwidth(figsize_x)

            ax1.title.set_text(title + str(pixels[i][0]) + " - L4: "+str(steps_45[j]) + "°")
            ax1.title.set_fontsize(20)

            ax1.plot(arr[i,j,:,0], arr[i,j,:,1], c="r", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[0]+1))
            ax2.plot(arr[i,j,:,0], arr[i,j,:,2], c="g", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[1]+1))
            ax3.plot(arr[i,j,:,0], arr[i,j,:,3], c="b", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[2]+1))

            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax2.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax3.yaxis.get_major_ticks():
                tick.label.set_fontsize(14) 
            for tick in ax3.xaxis.get_major_ticks():
                tick.label.set_fontsize(14)

            ax3.set_xlabel(xlabel, fontsize=18, labelpad=5)
            ax2.set_ylabel("Intensity (8 Bit)", fontsize=18, labelpad=10)

            plt.savefig(savepath_1 + str(pixels[i][0]) + savepath_2 +str(pixels[i][0]) + savepath_3 + str(steps_45[j])+".png", bbox_inches = 'tight', pad_inches = 0)
            plt.close()

# Function that displays the intensity graph for the given pixel for each channel and saves it as an image
# (Used for a setup containing a LP, a Quarterwave-retarder and a waveplate)            
def plotIntensity_L4_WP_LP(arr, pixels, title, xlabel, savepath_1, savepath_2, savepath_3, savepath_4):
    for k in range(0,len(steps_90)):
        for i in range(0,len(pixels)):
            for j in range(0,len(steps_45_2)):
                fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
                fig.set_figheight(figsize_y)
                fig.set_figwidth(figsize_x)

                ax1.title.set_text(title + str(pixels[i][0]) + " - LP: " + str(steps_90[k]) + "°" + " - L4: "+str(steps_45_2[j]) + "°")
                ax1.title.set_fontsize(20)

                ax1.plot(arr[k,i,j,:,0], arr[k,i,j,:,1], c="r", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[0]+1))
                ax2.plot(arr[k,i,j,:,0], arr[k,i,j,:,2], c="g", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[1]+1))
                ax3.plot(arr[k,i,j,:,0], arr[k,i,j,:,3], c="b", label="Pixel "+str(pixels[i][0])+" - Channel " + str(channels[2]+1))

                for tick in ax1.yaxis.get_major_ticks():
                    tick.label.set_fontsize(14) 
                for tick in ax2.yaxis.get_major_ticks():
                    tick.label.set_fontsize(14) 
                for tick in ax3.yaxis.get_major_ticks():
                    tick.label.set_fontsize(14) 
                for tick in ax3.xaxis.get_major_ticks():
                    tick.label.set_fontsize(14)

                ax3.set_xlabel(xlabel, fontsize=18, labelpad=5)
                ax2.set_ylabel("Intensity (8 Bit)", fontsize=18, labelpad=10)

                plt.savefig(savepath_1 + str(steps_90[k]) + savepath_2 + str(pixels[i][0]) + savepath_3 +str(pixels[i][0]) + savepath_4 + str(steps_45_2[j])+".png", bbox_inches = 'tight', pad_inches = 0)
                plt.close()


# ---------------------------------------- #
# Main Program
# ---------------------------------------- #
				
# Show Pixel Positions

folder_0 = "20190724/"
subfolder_0 = "exercise_2_with_sample/"
loc_0 = directory + folder_0 + subfolder_0 + "img_lp_10_wp_60" + appendix
image_0 = Image.open(loc_0)
ima_0 = np.array(image_0)

savename = "../Measurements/PixelPositions.png"
showPixels(image_0, pixels, savename)

for i in range(1,11):
    savename = "../Measurements/PixelPositions_Pixel_"+str(i)+".png"
    showPixelsDetailed(image_0, pixels, savename, i, 300)


# # --------------
# # Background
# # --------------

# ## Background Ex1 (LP_LP)

folder_1 = "20190724/"
subfolder_1 = "exercise_1_without_sample/"
res_1 = getIntensityGraph_LP(directory + folder_1 + subfolder_1, steps_10, pixels, channels)
res_1.shape

plotIntensity_LP(res_1, 
                 pixels, 
                 "Background - LP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Not_Shifted/Background/LP_LP/Channelwise/pixel_",
                 "-LP_LP.png")

np.save("Arrays/Not_Shifted/Background/PixelData_LP_LP", res_1)


# ## Background Ex2 (LP_WP_LP)

folder_2 = "20190724/"
subfolder_2 = "exercise_2_without_sample/"
res_2 = getIntensityGraph_LP_WP(directory + folder_2 + subfolder_2, steps_20, steps_10, pixels, channels)
res_2.shape

plotIntensity_WP_LP(res_2, 
                    pixels, 
                    "Background - LP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Not_Shifted/Background/LP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-LP_WP_LP-LP_")

np.save("Arrays/Not_Shifted/Background/PixelData_LP_WP_LP", res_2)


# ## Background Ex3 (CP_WP_LP)

folder_3_1 = "20190725/"
subfolder_3_1 = "exercise_3_without_sample_with_wp/"
res_3_1 = getIntensityGraph_LP_WP(directory + folder_3_1 + subfolder_3_1, steps_20, steps_10, pixels, channels)
res_3_1.shape

plotIntensity_WP_LP(res_3_1, 
                    pixels, 
                    "Background - CP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Not_Shifted/Background/CP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_WP_LP-LP_")

np.save("Arrays/Not_Shifted/Background/PixelData_CP_WP_LP", res_3_1)


# ## Background Ex3 (CP_LP)

folder_3_2 = "20190725/"
subfolder_3_2 = "exercise_3_without_sample_without_wp/"
res_3_2 = getIntensityGraph_LP(directory + folder_3_2 + subfolder_3_2, steps_10, pixels, channels)
res_3_2.shape

plotIntensity_LP(res_3_2, 
                 pixels, 
                 "Background - CP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Not_Shifted/Background/CP_LP/Channelwise/pixel_",
                 "-CP_LP.png")

np.save("Arrays/Not_Shifted/Background/PixelData_CP_LP", res_3_2)


# ## Background Ex4 (CP_L4_LP)

folder_4_1 = "20190725/"
subfolder_4_1 = "exercise_4_without_sample_with_L4/"
res_4_1 = getIntensityGraph_L4_LP(directory + folder_4_1 + subfolder_4_1, steps_45, steps_20, pixels, channels)
res_4_1.shape

plotIntensity_L4_LP(res_4_1, 
                    pixels, 
                    "Background - CP_L4_LP - Pixel ", 
                    "Angle of linear polarization filter [°]",
                    "Analysis/Not_Shifted/Background/CP_L4_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_L4_LP-L4_")

np.save("Arrays/Not_Shifted/Background/PixelData_CP_L4_LP", res_4_1)


# ## Background Ex4 (CP_L4_WP_LP)

folder_4_2 = "20190725/"
subfolder_4_2 = "exercise_4_without_sample_with_WP_ontop_L4/"
res_4_2 = getIntensityGraph_L4_WP_LP(directory + folder_4_2 + subfolder_4_2, steps_45_2, steps_40, steps_90, pixels, channels)
res_4_2.shape

plotIntensity_L4_WP_LP(res_4_2, 
                       pixels, 
                       "Background - CP_L4_WP_LP - Pixel ", 
                       "Angle of waveplate [°]",
                       "Analysis/Not_Shifted/Background/CP_L4_WP_LP/LP_",
                       "/Channelwise/Pixel_",
                       "/pixel_",
                       "-CP_L4_WP_LP-L4_")

np.save("Arrays/Not_Shifted/Background/PixelData_CP_L4_WP_LP", res_4_2)


# # --------------
# # Sample
# # --------------

# ## Sample Ex1 (LP_LP)

folder_1_s = "20190724/"
subfolder_1_s = "exercise_1_with_sample/"
res_1_s = getIntensityGraph_LP(directory + folder_1_s + subfolder_1_s, steps_10, pixels, channels)
res_1_s.shape

plotIntensity_LP(res_1_s, 
                 pixels, 
                 "Sample - LP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Not_Shifted/Sample/LP_LP/Channelwise/pixel_",
                 "-LP_LP.png")

np.save("Arrays/Not_Shifted/Sample/PixelData_LP_LP", res_1_s)


# ## Sample Ex2 (LP_WP_LP)

folder_2_s = "20190724/"
subfolder_2_s = "exercise_2_with_sample/"
res_2_s = getIntensityGraph_LP_WP(directory + folder_2_s + subfolder_2_s, steps_20, steps_10, pixels, channels)
res_2_s.shape

plotIntensity_WP_LP(res_2_s, 
                    pixels, 
                    "Sample - LP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Not_Shifted/Sample/LP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-LP_WP_LP-LP_")

np.save("Arrays/Not_Shifted/Sample/PixelData_LP_WP_LP", res_2_s)


# ## Sample Ex3 (CP_WP_LP)

folder_3_1_s = "20190724/"
subfolder_3_1_s = "exercise_3_with_sample_with_wp/"
res_3_1_s = getIntensityGraph_LP_WP(directory + folder_3_1_s + subfolder_3_1_s, steps_20, steps_10, pixels, channels)
res_3_1_s.shape

plotIntensity_WP_LP(res_3_1_s, 
                    pixels, 
                    "Sample - CP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Not_Shifted/Sample/CP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_WP_LP-LP_")

np.save("Arrays/Not_Shifted/Sample/PixelData_CP_WP_LP", res_3_1_s)


# ## Sample Ex3 (CP_LP)

folder_3_2_s = "20190724/"
subfolder_3_2_s = "exercise_3_with_sample_without_wp/"
res_3_2_s = getIntensityGraph_LP(directory + folder_3_2_s + subfolder_3_2_s, steps_10, pixels, channels)
res_3_2_s.shape

plotIntensity_LP(res_3_2_s, 
                 pixels, 
                 "Sample - CP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Not_Shifted/Sample/CP_LP/Channelwise/pixel_",
                 "-CP_LP.png")

np.save("Arrays/Not_Shifted/Sample/PixelData_CP_LP", res_3_2_s)


# ## Sample Ex4 (CP_L4_LP)

folder_4_1_s = "20190725/"
subfolder_4_1_s = "exercise_4_with_sample_with_L4/"
res_4_1_s = getIntensityGraph_L4_LP_sample(directory + folder_4_1_s + subfolder_4_1_s, steps_45, steps_20, pixels, channels)
res_4_1_s.shape

plotIntensity_L4_LP(res_4_1_s, 
                    pixels, 
                    "Sample - CP_L4_LP - Pixel ", 
                    "Angle of linear polarization filter [°]",
                    "Analysis/Not_Shifted/Sample/CP_L4_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_L4_LP-L4_")

np.save("Arrays/Not_Shifted/Sample/PixelData_CP_L4_LP", res_4_1_s)


# ## Sample Ex4 (CP_L4_WP_LP)

folder_4_2_s = "20190725/"
subfolder_4_2_s = "exercise_4_with_sample_with_WP_ontop_L4/"
res_4_2_s = getIntensityGraph_L4_WP_LP(directory + folder_4_2_s + subfolder_4_2_s, steps_45_2, steps_40, steps_90, pixels, channels)
res_4_2_s.shape

plotIntensity_L4_WP_LP(res_4_2_s, 
                       pixels, 
                       "Sample - CP_L4_WP_LP - Pixel ", 
                       "Angle of waveplate [°]",
                       "Analysis/Not_Shifted/Sample/CP_L4_WP_LP/LP_",
                       "/Channelwise/Pixel_",
                       "/pixel_",
                       "-CP_L4_WP_LP-L4_")

np.save("Arrays/Not_Shifted/Sample/PixelData_CP_L4_WP_LP", res_4_2_s)


# # --------------
# # Difference
# # --------------

# ## Difference Ex1 (LP_LP)

res_1_d = res_1 - res_1_s
res_1_d[:,:,0] = res_1[:,:,0]

plotIntensity_LP(res_1_d, 
                 pixels, 
                 "Difference - LP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Not_Shifted/Difference/LP_LP/Channelwise/pixel_",
                 "-LP_LP.png")

np.save("Arrays/Not_Shifted/Difference/PixelData_LP_LP", res_1_d)


# ## Difference Ex2 (LP_WP_LP)

res_2_d = res_2 -res_2_s
res_2_d[:,:,:,0] = res_2[:,:,:,0]

plotIntensity_WP_LP(res_2_d, 
                    pixels, 
                    "Difference - LP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Not_Shifted/Difference/LP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-LP_WP_LP-LP_")

np.save("Arrays/Not_Shifted/Difference/PixelData_LP_WP_LP", res_2_d)


# ## Difference Ex3 (CP_WP_LP)

res_3_1_d = res_3_1 -res_3_1_s
res_3_1_d[:,:,:,0] = res_3_1[:,:,:,0]

plotIntensity_WP_LP(res_3_1_d, 
                    pixels, 
                    "Difference - CP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Not_Shifted/Difference/CP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_WP_LP-LP_")

np.save("Arrays/Not_Shifted/Difference/PixelData_CP_WP_LP", res_3_1_d)


# ## Difference Ex3 (CP_LP)

res_3_2_d = res_3_2 - res_3_2_s
res_3_2_d[:,:,0] = res_3_2[:,:,0]

plotIntensity_LP(res_3_2_d, 
                 pixels, 
                 "Difference - CP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Not_Shifted/Difference/CP_LP/Channelwise/pixel_",
                 "-CP_LP.png")

np.save("Arrays/Not_Shifted/Difference/PixelData_CP_LP", res_3_2_d)


# ## Difference Ex4 (CP_L4_LP)

res_4_1_d = res_4_1 - res_4_1_s
res_4_1_d[:,:,:,0] = res_4_1[:,:,:,0]

plotIntensity_L4_LP(res_4_1_d, 
                    pixels, 
                    "Difference - CP_L4_LP - Pixel ", 
                    "Angle of linear polarization filter [°]",
                    "Analysis/Not_Shifted/Difference/CP_L4_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_L4_LP-L4_")

np.save("Arrays/Not_Shifted/Difference/PixelData_CP_L4_LP", res_4_1_d)


# ## Difference Ex4 (CP_L4_WP_LP)

res_4_2_d = res_4_2 - res_4_2_s
res_4_2_d[:,:,:,:,0] = res_4_2[:,:,:,:,0]


plotIntensity_L4_WP_LP(res_4_2_d, 
                       pixels, 
                       "Difference - CP_L4_WP_LP - Pixel ", 
                       "Angle of waveplate [°]",
                       "Analysis/Not_Shifted/Difference/CP_L4_WP_LP/LP_",
                       "/Channelwise/Pixel_",
                       "/pixel_",
                       "-CP_L4_WP_LP-L4_")

np.save("Arrays/Not_Shifted/Difference/PixelData_CP_L4_WP_LP", res_4_2_d)


# # --------------
# # Shifted - Sample
# # --------------

folder_1_s_shift = "20190724/"
subfolder_1_s_shift = "exercise_1_with_sample/"
res_1_s_shift = getIntensityGraph_LP(directory_shift + folder_1_s_shift + subfolder_1_s_shift, steps_10, pixels, channels)
res_1_s_shift.shape

plotIntensity_LP(res_1_s_shift, 
                 pixels, 
                 "Sample - LP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Shifted/Sample/LP_LP/Channelwise/pixel_",
                 "-LP_LP.png")

np.save("Arrays/Shifted/Sample/PixelData_LP_LP", res_1_s_shift)


folder_2_s_shift = "20190724/"
subfolder_2_s_shift = "exercise_2_with_sample/"
res_2_s_shift = getIntensityGraph_LP_WP(directory_shift + folder_2_s_shift + subfolder_2_s_shift, steps_20, steps_10, pixels, channels)
res_2_s_shift.shape

plotIntensity_WP_LP(res_2_s_shift, 
                    pixels, 
                    "Sample - LP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Shifted/Sample/LP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-LP_WP_LP-LP_")

np.save("Arrays/Shifted/Sample/PixelData_LP_WP_LP", res_2_s_shift)


folder_3_1_s_shift = "20190724/"
subfolder_3_1_s_shift = "exercise_3_with_sample_with_wp/"
res_3_1_s_shift = getIntensityGraph_LP_WP(directory_shift + folder_3_1_s_shift + subfolder_3_1_s_shift, steps_20, steps_10, pixels, channels)
res_3_1_s_shift.shape

plotIntensity_WP_LP(res_3_1_s_shift, 
                    pixels, 
                    "Sample - CP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Shifted/Sample/CP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_WP_LP-LP_")

np.save("Arrays/Shifted/Sample/PixelData_CP_WP_LP", res_3_1_s_shift)


folder_3_2_s_shift = "20190724/"
subfolder_3_2_s_shift = "exercise_3_with_sample_without_wp/"
res_3_2_s_shift = getIntensityGraph_LP(directory_shift + folder_3_2_s_shift + subfolder_3_2_s_shift, steps_10, pixels, channels)
res_3_2_s_shift.shape

plotIntensity_LP(res_3_2_s_shift, 
                 pixels, 
                 "Sample - CP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Shifted/Sample/CP_LP/Channelwise/pixel_",
                 "-CP_LP.png")

np.save("Arrays/Shifted/Sample/PixelData_CP_LP", res_3_2_s_shift)


folder_4_1_s_shift = "20190725/"
subfolder_4_1_s_shift = "exercise_4_with_sample_with_L4/"
res_4_1_s_shift = getIntensityGraph_L4_LP_sample(directory_shift + folder_4_1_s_shift + subfolder_4_1_s_shift, steps_45, steps_20, pixels, channels)
res_4_1_s_shift.shape

plotIntensity_L4_LP(res_4_1_s_shift, 
                    pixels, 
                    "Sample - CP_L4_LP - Pixel ", 
                    "Angle of linear polarization filter [°]",
                    "Analysis/Shifted/Sample/CP_L4_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_L4_LP-L4_")

np.save("Arrays/Shifted/Sample/PixelData_CP_L4_LP", res_4_1_s_shift)


folder_4_2_s_shift = "20190725/"
subfolder_4_2_s_shift = "exercise_4_with_sample_with_WP_ontop_L4/"
res_4_2_s_shift = getIntensityGraph_L4_WP_LP(directory_shift + folder_4_2_s_shift + subfolder_4_2_s_shift, steps_45_2, steps_40, steps_90, pixels, channels)
res_4_2_s_shift.shape

plotIntensity_L4_WP_LP(res_4_2_s_shift, 
                       pixels, 
                       "Sample - CP_L4_WP_LP - Pixel ", 
                       "Angle of waveplate [°]",
                       "Analysis/Shifted/Sample/CP_L4_WP_LP/LP_",
                       "/Channelwise/Pixel_",
                       "/pixel_",
                       "-CP_L4_WP_LP-L4_")

np.save("Arrays/Shifted/Sample/PixelData_CP_L4_WP_LP", res_4_2_s_shift)


# # --------------
# # Shifted - Difference
# # --------------

res_1_d_shift = res_1 - res_1_s_shift
res_1_d_shift[:,:,0] = res_1[:,:,0]

plotIntensity_LP(res_1_d_shift, 
                 pixels, 
                 "Difference - LP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Shifted/Difference/LP_LP/Channelwise/pixel_",
                 "-LP_LP.png")

np.save("Arrays/Shifted/Difference/PixelData_LP_LP", res_1_d_shift)


res_2_d_shift = res_2 -res_2_s_shift
res_2_d_shift[:,:,:,0] = res_2[:,:,:,0]

plotIntensity_WP_LP(res_2_d_shift, 
                    pixels, 
                    "Difference - LP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Shifted/Difference/LP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-LP_WP_LP-LP_")

np.save("Arrays/Shifted/Difference/PixelData_LP_WP_LP", res_2_d_shift)


res_3_1_d_shift = res_3_1 -res_3_1_s_shift
res_3_1_d_shift[:,:,:,0] = res_3_1[:,:,:,0]

plotIntensity_WP_LP(res_3_1_d_shift, 
                    pixels, 
                    "Difference - CP_WP_LP - Pixel ", 
                    "Angle of waveplate [°]",
                    "Analysis/Shifted/Difference/CP_WP_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_WP_LP-LP_")

np.save("Arrays/Shifted/Difference/PixelData_CP_WP_LP", res_3_1_d_shift)


res_3_2_d_shift = res_3_2 - res_3_2_s_shift
res_3_2_d_shift[:,:,0] = res_3_2[:,:,0]

plotIntensity_LP(res_3_2_d_shift, 
                 pixels, 
                 "Difference - CP_LP - Pixel ", 
                 "Angle of linear polarization filter [°]",
                 "Analysis/Shifted/Difference/CP_LP/Channelwise/pixel_",
                 "-CP_LP.png")

np.save("Arrays/Shifted/Difference/PixelData_CP_LP", res_3_2_d_shift)


res_4_1_d_shift = res_4_1 - res_4_1_s_shift
res_4_1_d_shift[:,:,:,0] = res_4_1[:,:,:,0]

plotIntensity_L4_LP(res_4_1_d_shift, 
                    pixels, 
                    "Difference - CP_L4_LP - Pixel ", 
                    "Angle of linear polarization filter [°]",
                    "Analysis/Shifted/Difference/CP_L4_LP/Channelwise/Pixel_",
                    "/pixel_",
                    "-CP_L4_LP-L4_")

np.save("Arrays/Shifted/Difference/PixelData_CP_L4_LP", res_4_1_d_shift)


res_4_2_d_shift = res_4_2 - res_4_2_s_shift
res_4_2_d_shift[:,:,:,:,0] = res_4_2[:,:,:,:,0]


plotIntensity_L4_WP_LP(res_4_2_d_shift, 
                       pixels, 
                       "Difference - CP_L4_WP_LP - Pixel ", 
                       "Angle of waveplate [°]",
                       "Analysis/Shifted/Difference/CP_L4_WP_LP/LP_",
                       "/Channelwise/Pixel_",
                       "/pixel_",
                       "-CP_L4_WP_LP-L4_")

np.save("Arrays/Shifted/Difference/PixelData_CP_L4_WP_LP", res_4_2_d_shift)

